import boto3
import json


with open("config.json", "r") as jsonfile:
    config = json.load(jsonfile)

key_pair_name = config["key_pair_name"]
security_group = config["security_group_name"]
availabilityzones = config["availabilityzone"]
data = config["user_data"]


def create_key_pair(key_pair_name):
    global private_key
    print(key_pair_name)
    ec2_client = boto3.client("ec2")
    for key_pair in ec2_client.describe_key_pairs(DryRun=False)['KeyPairs']:
        if key_pair['KeyName'] == str(key_pair_name):
            print("key_pair is already present")
            return key_pair['KeyName']
    # Key-pair was not found, create it
    key_pair = ec2_client.create_key_pair(KeyName=str(key_pair_name))
    print(key_pair)
    private_key = key_pair["KeyMaterial"]
    print(private_key, file=open('{}.pem'.format(key_pair_name), 'w'))
    print(private_key)
    return key_pair['KeyName']


def create_check_security_group(security_group):
    ec2 = boto3.client('ec2')
    for rds_security_group in ec2.describe_security_groups()['SecurityGroups']:
        if rds_security_group['GroupName'] == str(security_group):
            print(rds_security_group['GroupId'])
            return rds_security_group['GroupId']
    # Security Group was not found, create it
    rds_security_group_name = ec2.create_security_group(
        GroupName=str(security_group),
        Description='mongodb-security-group')
    print(rds_security_group_name['GroupId'])
    ec2.authorize_security_group_ingress(
        GroupId=rds_security_group_name['GroupId'],
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'icmp',
             'FromPort': -1,
             'ToPort': -1,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'tcp',
             'FromPort': 27017,
             'ToPort': 27017,
             'UserIdGroupPairs': [{'GroupId': rds_security_group_name['GroupId']}],
             }
        ])
    print(rds_security_group_name['GroupId'])
    return rds_security_group_name['GroupId']


def create_instance(security_group_id, availabilityzone, key_name):
    ec2_client = boto3.client("ec2")
    response = ec2_client.run_instances(
        BlockDeviceMappings=[
            {
                'DeviceName': '/dev/sda1',
                'Ebs': {
                    'DeleteOnTermination': True,
                    'VolumeSize': 10,
                    'VolumeType': 'gp2'
                },
            },
        ],
        Placement={
            'AvailabilityZone': str(availabilityzone)
        },
        KeyName=str(key_name),
        ImageId='ami-0b28dfc7adc325ef4',
        UserData=data,
        InstanceType='t2.micro',
        MaxCount=1,
        MinCount=1,
        Monitoring={
            'Enabled': False
        },
        SecurityGroupIds=[
            str(security_group_id),
        ],
    )

    print(response["Instances"][0]["InstanceId"])


def main():
    key_name = create_key_pair(key_pair_name)
    security_group_id = create_check_security_group(security_group)
    for availabilityzone in availabilityzones:
        instance_id = create_instance(security_group_id, availabilityzone, key_name)


if __name__ == "__main__":
    main()
