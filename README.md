# mongodb_replicaset

## Prerequists
Setup client through aws configure
for more details follow below steps
```
aws configure
<access_key>
<secret_key>
<region>

```

## Getting started

run the script "assignment.py" it will create below resources on AWS
    1. Key-pair if not exist
	2. security group & security group inbound rules
	3. ec2 instances 
	4. mongodb installation, adding few parameters on kernal level & setup of certs along with keys

```
python assignment.py
```

## once script is triggered wait for few minutes till mongodb came up fully after that run below commands.

```
mongo `hostname` --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem --sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem
	use admin
	db.createUser({ user: "mongoadmin" , pwd: "mongoadmin", roles: [{"role": "root", "db": "admin"}]})
	db.auth("mongoadmin", "mongoadmin")
	exit()
```

## open /etc/mongod.conf & remove comments of security & replica set part then restart mongod service & run below command

```
mongo -u mongoadmin -p mongoadmin mongo1.domain.net/admin --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem --sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem --eval 'rs.initiate({_id: "TestRS-0",members: [{ _id: 0, host : "mongo1.domain.net:27017" }, { _id: 1, host : "mongo2.domain.net:27017" }, { _id: 2, host : "mongo3.domain.net:27017", "arbiterOnly": true}]})'
```


